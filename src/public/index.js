//acceso al socket
const socket = io()
var click = false
var moving_mouse = false
var x_position = 0;
var y_position = 0;
var previous_position = null
var color = 'black'

const canvas = document.getElementById('canvas')
const context = canvas.getContext('2d')

const width = window.innerWidth
const height = window.innerHeight

canvas.width = width
canvas.height = height

canvas.addEventListener('mousedown', () => {
    console.log('Esta dando click')
    click = true
})

canvas.addEventListener('mouseup', () => {
    console.log('No esta dando click')
    click = false
})

canvas.addEventListener('mousemove', (e) => {
    x_position = e.clientX
    y_position = e.clientY
    moving_mouse = true
})

function create_drawing() {
    if (click && moving_mouse && previous_position != null) {
        let drawing = {
            x_position: x_position,
            y_position: y_position,
            color: color,
            previous_position: previous_position
        }
        show_drawing(drawing)
    }
    previous_position = { x_position: x_position, y_position: y_position }
    setTimeout(create_drawing, 25)
}

function show_drawing(drawing) {
    context.beginPath() //dibuja los trazos
    context.lineWidth = 3 //ancho de la linea
    context.strokeStyle = drawing.color //define el color de la linea
    context.moveTo(drawing.x_position, drawing.y_position) //define la posicion inicial
    context.lineTo(drawing.previus_position.x_position,
        drawing.previous_position.y_position)
    context.stroke() //realiza el trazo
}

create_drawing()